local inclusions = [
  { "template": "SAST.gitlab-ci.yml" }
];

local nodejs_scan_job(path) =
  {
    extends: [
      ".sast-analyzer",
      "nodejs-scan-sast",
    ],
    variables: {
      "CI_PROJECT_DIR": path,
      "SECURE_LOG_LEVEL": "debug",
      "SAST_EXCLUDED_PATHS": "" // TEMP: until https://gitlab.com/gitlab-org/gitlab/-/issues/223283
    },
    rules: [
      { "if": "$CI_COMMIT_BRANCH" }
    ],
    artifacts: {
      paths: [
        path + "/gl-sast-report.json"
      ],
      reports: {
        sast: path + "/gl-sast-report.json"
      }
    }
  };

{
  include: inclusions,
} + {
  ["sast-" + job]: nodejs_scan_job(job)
  for job in std.split(std.extVar("ANALYZER_DIRS"), ",")
}

